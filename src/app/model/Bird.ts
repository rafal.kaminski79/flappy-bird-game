import { Pixel } from './Pixel';

export class Bird implements Pixel {
  y: number = 4;
  getColor(): string {
    return 'green';
  }

  fall() {
    if (this.y < 9) {
      this.y = this.y + 1;
    }
  }

  fly() {
    if (this.y > 0) {
      this.y = this.y - 1;
    }
  }
}
