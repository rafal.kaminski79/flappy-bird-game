import { Bird } from "./Bird";
import { Column } from "./Column";

export interface BoardState {
    bird: Bird;
    board: Column[];    
}