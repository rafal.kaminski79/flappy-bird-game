import { Pixel } from "./Pixel";

export class Obstacle implements Pixel {
    getColor(): string {
       return "grey";
    }

}