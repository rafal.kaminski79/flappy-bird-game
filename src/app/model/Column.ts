import { Bird } from "./Bird"
import { Free } from "./Free"
import { Obstacle } from "./Obstacle"

export class Column {
    cells: Array<Obstacle | Free | Bird> = [];
}