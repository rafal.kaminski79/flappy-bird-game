import { Component, OnInit } from '@angular/core';
import {
  combineLatestWith,
  fromEvent,
  interval,
  map,
  merge,
  Observable,
  scan,
} from 'rxjs';
import { Bird } from './model/Bird';
import { BoardState } from './model/BoardState';
import { Column } from './model/Column';
import { Free } from './model/Free';
import { Obstacle } from './model/Obstacle';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  private readonly title = 'flappy-bird-game';
  private readonly size = 10;
  public state$: Observable<BoardState> = new Observable();
  private board$: Observable<Column[]> = interval(2000).pipe(
    scan((acc) => this.moveBoard(acc), this.initBoard())
  );
  private bird$: Observable<Bird> = merge(
    interval(300),
    fromEvent(document, 'keydown')
  ).pipe(
    scan((acc, curr) => {
      curr instanceof KeyboardEvent ? acc.fly() : acc.fall();
      return acc;
    }, new Bird())
  );
  constructor() {}

  ngOnInit(): void {
    this.state$ = this.board$.pipe(
      combineLatestWith(this.bird$),
      map(([board, bird]) => this.putBird(board, bird))
    );
  }

  private putBird(board: Column[], bird: Bird): BoardState {
    const firstColumn: Column = {
      cells: board[0].cells.filter((x) => x instanceof Bird == false),
    };
    firstColumn.cells.length >= this.size
      ? firstColumn.cells.splice(bird.y, 1, bird)
      : firstColumn.cells.splice(bird.y, 0, bird);
    board[0] = firstColumn;
    return {
      board: board,
      bird: bird,
    };
  }

  private initBoard(): Column[] {
    let board = new Array<Column>();
    for (let i = 0; i < this.size; i++) {
      i == this.size - 1 || i == this.size - 6
        ? board.push(this.generateObstacle())
        : board.push(this.generateFreeColumn());
    }
    return board;
  }

  private moveBoard(board: Column[]): Column[] {
    board.filter((col) => col.cells.some((x) => x instanceof Obstacle))
      .length == 1
      ? board.push(this.generateObstacle())
      : board.push(this.generateFreeColumn());
    board.shift();
    return board;
  }

  generateFreeColumn(): Column {
    const column = new Column();
    for (let j = 0; j < this.size; j++) {
      column.cells.push(new Free());
    }
    return column;
  }

  private generateObstacle(): Column {
    const gapStart = Math.floor(Math.random() * (this.size - 3));
    const column = new Column();
    for (let j = 0; j < this.size; j++) {
      j <= gapStart || j > gapStart + 3
        ? column.cells.push(new Obstacle())
        : column.cells.push(new Free());
    }
    return column;
  }
}
